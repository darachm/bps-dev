# Bacterial Positioning System

# Todo list

old todo list, see issues:

- more informative error if you dont have plasmid-specific extraciton isntructions
- write docs, Sphinx I think like Nextflow's main repo use that model
- write a mermaid diagram to explain the config's expressivity, where data
    flows and how and why
- how to deduplicate the filter FASTA file?

# BPS analysis

Read the paper (link here)

[Documentation here](darachm.gitlab.io/bps-dev) 
for both [benchwork](https://darachm.gitlab.io/bps-dev/wetbench.html) and 
[analysis pipeline](https://darachm.gitlab.io/bps-dev/pipeline.html) 
